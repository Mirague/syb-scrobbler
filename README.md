# Scrobbler

Interview task for Soundtrack Your Brand by Melvin Valster. 

## Demo

View at https://syb-scrobbler-melvin.herokuapp.com/

## Assignment

The original assignment can be found at https://gist.github.com/mogelbrod/9992b7653b5fec4c036a0fc734a66596.

    Your objective is to improve the existing presentation and code base in any way you like. You do not need to look into scrobble-api.js.
    Possible improvements include:

    * Add some style, preferably without integrating an entire CSS framework like bootstrap
    * Make the list responsive
    * Add animations (loading spinner, fade/slide in scrobbles, etc.)
    * Fade in album art when loaded, rendering a placeholder using scrobble.colors.(primary|accent)
    * Display the currently playing track with cover art in one column, and a track history in another
    * Only display the currently playing track in a kiosk-like mode
    * Rotate/carousel through the scrobbles, displaying one at a time with a nice presentation
    * Linkify tracks/artists using uri/artists[i].uri from the scrobble records
    * Utilize any other data available in each scrobble (duration_ms, channel_name, uri, etc.)
    * Display play time in a relative format (X seconds/minutes/hours ago)
    * Re-implement using your favorite JS framework, or try a new one

## Backlog of possible improvements

1. Kiosk: Swipe to move between tracks instead of the controls on mobile.

## Getting up and running

1. `yarn install`
2. `yarn start`

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

