import React, { Component } from 'react';
import ScrobbleApi from './libs/scrobble-api';
import { isTrackPlayingNow } from './libs/utils';
import moment from 'moment';

class ScrobbleListItem extends Component {
  onCoverLoad(e) {
    e.target.className = 'loaded';
  }

  renderArtists(artists) {
    return artists.map(a => {
      return <a key={a.name} href={a.uri} title="Open in Spotify">{a.name}</a>;
    }).reduce((prev, curr) => [prev, ', ', curr]);
  }

  render() {
    const { scrobble } = this.props;
    const playedAt = moment(ScrobbleApi.playDate(scrobble));
    const nowPlaying = this.props.index === 0 && isTrackPlayingNow(scrobble); 
    const artists = this.renderArtists(scrobble.artists);
    
    return (
      <tr>
        <td className="played__cover">
          <div className="played__cover" style={{ backgroundColor: scrobble.colors.primary }}>
            <img alt="" src={scrobble.image_url} onLoad={this.onCoverLoad} />
          </div>
        </td>
        <td className="played__title">
          <a href={scrobble.uri} title="Open in Spotify">{scrobble.song_name}</a>
          <div className="played__artists-mobile">{artists}</div>
        </td>
        <td className="played__artists">{artists}</td>
        <td className="played__time" title={playedAt.format('LLL')}>
          {nowPlaying ? 'Now playing' : playedAt.fromNow()}
        </td>
      </tr>
    );
  }
}

export default ScrobbleListItem;
