import ScrobbleApi from './scrobble-api';
import moment from 'moment';

export const momentFromNow = (scrobble) => {
  return moment(ScrobbleApi.playDate(scrobble)).fromNow();
};

/**
 * Seeing as updates from the server don't seem to come instantly, the most recent track will be 
 * around longer than its `duration_ms`, making this an unreliable way to check if the most recent
 * track is actually playing. This method aims to guess if the track is currently playing.
 * 
 * @param {*} scrobble 
 */
export const isTrackPlayingNow = (scrobble) => {
  const playedAt = moment(ScrobbleApi.playDate(scrobble));
  const threshold = 2 * 60 * 1000; // 2 minutes in ms
  return moment().valueOf() - playedAt.valueOf() < (scrobble.duration_ms + threshold);
};