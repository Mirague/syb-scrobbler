import React, { Component } from 'react';
import ScrobbleApi from './libs/scrobble-api';
import ScrobbleList from './ScrobbleList';
import Loader from './Loader';
import Kiosk from './Kiosk';

// This Zone only plays songs Monday - Friday
const zoneIdMonFri = 'U291bmRab25lLCwxano5YXYzcjd5OC9Mb2NhdGlvbiwsMWptZjV1aTBrNWMvQWNjb3VudCwsMW5kbWR6bmF5Z3cv';
// This Zone always plays and is used for QA
const zoneIdQA = 'U291bmRab25lLCwxano4Z2s5M2pzdy9Mb2NhdGlvbiwsMWs4ZXUxb2ZhcHMvQWNjb3VudCwsMWtrMDgzY21pbzAv';

class App extends Component {
  constructor() {
    super(...arguments);
    this.addScrobble = this.addScrobble.bind(this);
    this.state = {
      isFetching: true,
      scrobbles: [],
      mode: (localStorage && localStorage.getItem('lastMode')) || 'list'
    }
  }

  componentDidMount() {
    const zoneId = (localStorage && localStorage.getItem('zoneId')) || zoneIdQA;
    const api = new ScrobbleApi(zoneId);
    api.fetchHistory().then(scrobbles => {
      scrobbles.map(this.addScrobble);
      api.subscribe(this.addScrobble);
      this.setState({
        isFetching: false,
        scrobbles: this.state.scrobbles
      });
      if (localStorage) {
        localStorage.setItem('zoneId', zoneId);
      }
    });
  }

  setMode(mode) {
    if (localStorage) {
      localStorage.setItem('lastMode', mode);
    }
    this.setState(Object.assign({}, this.state, { mode }));
  }

  addScrobble(scrobble) {
    this.setState(Object.assign({}, this.state, {
      scrobbles: [scrobble].concat(this.state.scrobbles)
    }));
  }

  renderLoading() {
    return (
      <Loader />
    );
  }

  renderKiosk() {
    return (
      <div className="kiosk-container">
        <Kiosk scrobbles={this.state.scrobbles} exit={() => this.setMode('list')} />
      </div>
    );
  }

  renderList() {
    return (
      <div className="list">
        <div className="container">
          <button className="btn" onClick={() => this.setMode('kiosk')}>Kiosk mode</button>
          <h1>Now Playing</h1>
          <ScrobbleList scrobbles={this.state.scrobbles} />
        </div>
      </div>
    );
  }

  render() {
    if (this.state.isFetching) {
      return this.renderLoading();
    } else if (this.state.mode === 'list') {
      return this.renderList();
    } else if (this.state.mode === 'kiosk') {
      return this.renderKiosk();
    }
  }
}

export default App;
