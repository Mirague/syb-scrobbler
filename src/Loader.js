import React from 'react';
import './Loader.css';

const Loader = () => (
  <div className="loading aspect-view">
    <div className="loading__spinner"></div>
    <div className="loading__text">Loading Scrobbles...</div>
  </div>
);

export default Loader;
