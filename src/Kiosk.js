import React, { Component } from 'react';
import { momentFromNow, isTrackPlayingNow } from './libs/utils';
import './Kiosk.css';

class Kiosk extends Component {
  state = {
    index: 0,
    hideUI: false,
    imageLoaded: null
  }

  componentDidMount() {
    this.loadCoverImage();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.scrobbles.length !== nextProps.scrobbles.length) {
      // Keep the currently selected index intact
      if (this.state.index > 0) {
        this.setState(Object.assign({}, this.state, { index: this.state.index + 1 }));
      }
    }
  }

  componentDidUpdate() {
    const scrobble = this.props.scrobbles[this.state.index];
    if (this.state.imageLoaded !== scrobble.image_url) {
      this.loadCoverImage();
    }
  }

  componentWillUnmount() {
    clearTimeout(this._mouseMoveTimeout);
  }

  loadCoverImage() {
    const scrobble = this.props.scrobbles[this.state.index];
    const img = new Image();
    img.onload = () => {
      this.setState(Object.assign({}, this.state, { imageLoaded: scrobble.image_url }));
    };
    img.src = scrobble.image_url;
  }

  _onActivityEvent() {
    this.setState(Object.assign({}, this.state, { hideUI: false }));
    clearTimeout(this._mouseMoveTimeout);
    this._mouseMoveTimeout = setTimeout(() => {
      this.setState(Object.assign({}, this.state, { hideUI: true }));
    }, 2500);
  }

  previous() {
    if (this.state.index > 0) {
      this.setState(Object.assign({}, this.state, { index: this.state.index - 1 }));
    }
  }

  next() {
    if (this.state.index < this.props.scrobbles.length - 1) {
      this.setState(Object.assign({}, this.state, { index: this.state.index + 1 }));
    }
  }

  renderArtists(artists) {
    return artists.map(a => {
      return <a key={a.name} href={a.uri} title="Open in Spotify">{a.name}</a>;
    }).reduce((prev, curr) => [prev, ', ', curr]);
  }

  render() {
    const { scrobbles } = this.props;
    const { hideUI } = this.state;
    const scrobble = scrobbles[this.state.index];
    const { colors } = scrobble;
    const bgGradient = `radial-gradient(circle at 50% 40%, ${colors.primary} 20%, ${colors.accent} 100%)`;
    const bgImage = `url(${scrobble.image_url})`;
    const hasImageLoaded = this.state.imageLoaded === scrobble.image_url;
    const isPlaying = this.state.index === 0 && isTrackPlayingNow(scrobble);

    return (
      <div className={`kiosk aspect-view ${hideUI ? 'ui-hidden' : ''}`} onMouseMove={() => this._onActivityEvent()}>
        <div className="kiosk__exit-btn" onClick={() => this.props.exit()} title="Exit Kiosk mode">
          <i className="icon-close"></i>
        </div>

        <div className="kiosk__bg aspect-view">
          <div className="kiosk__bg-gradient aspect-view" style={{ backgroundImage: bgGradient }}></div>
          {hasImageLoaded &&
            <div className="kiosk__bg-cover aspect-view" style={{ backgroundImage: bgImage }}></div>
          }
        </div>

        <div className="kiosk__body">
          <h6>{scrobble.play_from.name}</h6>
          <div className="kiosk__cover" style={{ backgroundColor: colors.primary }}>
            {hasImageLoaded &&
              <div className="kiosk__cover-img" style={{ backgroundImage: bgImage }} />
            }
            <div className="kiosk__cover-spacer" />
            <div className={`kiosk__playing-badge ${this.state.index === 0 ? 'playing-now' : ''}`}>
              {isPlaying && 
                <span><i className="icon-note" /> Now playing</span>
              }
              {!isPlaying && 
                <span>{momentFromNow(scrobble)}</span>
              }
            </div>
          </div>

          <div className="kiosk__controls">
            {this.state.index > 0 &&
              <div className="kiosk__control previous" onClick={() => this.previous()} title="Show more recent track">
                <i className="icon-left" />
              </div>
            }
            {this.state.index < scrobbles.length - 1 &&
              <div className="kiosk__control next" onClick={() => this.next()} title="Show older track">
                <i className="icon-right" />
              </div>
            }
          </div>

          <div className="kiosk__meta">
            <h2 className="kiosk__title">
              <a href={scrobble.uri} title="Open in Spotify">{scrobble.song_name}</a>
            </h2>
            <div className="kiosk__artist">
              {this.renderArtists(scrobble.artists)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Kiosk;
