import React, { Component } from 'react';
import ScrobbleListItem from './ScrobbleListItem';
import { isTrackPlayingNow } from './libs/utils';
import './ScrobbleList.css';

class ScrobbleList extends Component {
  render() {
    const { scrobbles } = this.props;
    const hasPlayingTrack = isTrackPlayingNow(scrobbles[0]);
    return (
      <div className={`list-wrapper ${hasPlayingTrack ? 'has-playing' : ''}`}>
        <table className="played" cellSpacing="0">
          <thead>
            <tr>
              <th className="played__cover"></th>
              <th className="played__title"></th>
              <th className="played__artists">Artist</th>
              <th className="played__time">Played</th>
            </tr>
            <tr className="played__spacer" />
          </thead>
          <tbody>
            {scrobbles.map((scrobble, i) => [
              <ScrobbleListItem key={scrobble.id} scrobble={scrobble} index={i} />,
              <tr key={`${scrobble.id}-spacer`} className="played__spacer" />
            ])}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ScrobbleList;
